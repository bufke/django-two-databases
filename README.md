# Django with two databases

1. `docker compose up`
2. `docker compose exec -it web bash`

Django models live in default

- Migrate the default DB and foo app `./manage.py migrate`
- Migrate the "bar" app/db only `./manage.py migrate --database bar`

# Set up postgres fdw

Since we use the same database and user, skip as many configuration details as possible

In the postgres database:

1. `CREATE SERVER bar FOREIGN DATA WRAPPER postgres_fdw OPTIONS (dbname 'bar');`
1. `CREATE USER MAPPING FOR CURRENT_USER SERVER bar;`
1. `IMPORT FOREIGN SCHEMA public EXCEPT (django_migrations) FROM SERVER bar INTO public;`

It can also be created in reverse. Notice we have to exclude the foreign table manually

```sql
CREATE SERVER postgres FOREIGN DATA WRAPPER postgres_fdw OPTIONS (dbname 'postgres');
CREATE USER MAPPING FOR CURRENT_USER SERVER postgres;
IMPORT FOREIGN SCHEMA public EXCEPT (bar_bar, django_migrations) FROM SERVER postgres INTO public;
```

# Test it out

```python
from bar.models import *
from foo.models import *
Bar.objects.create()
Foo.objects.create()
```

Now it's time to sin!

```sql
select * from foo_foo left join bar_bar on bar_bar.id=foo_foo.id;
```

# It gets worse

Create a user and assign a bar.user to it

```python
Bar.objects.filter(user=user).first().user.pk
```

That isn't a foreign key, it's an abomination.
