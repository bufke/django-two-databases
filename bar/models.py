from django.contrib.auth.models import User
from django.db import models


class Bar(models.Model):
    name = models.CharField(default="the bar")
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        managed = False
