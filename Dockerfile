FROM python:3.12
ENV PYTHONUNBUFFERED=1 \
  PORT=8080 \
  POETRY_VIRTUALENVS_CREATE=false

RUN mkdir /code
WORKDIR /code

RUN pip install poetry
COPY pyproject.toml /code/
RUN poetry install --no-interaction --no-ansi

COPY . /code/

