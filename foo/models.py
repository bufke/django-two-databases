from django.db import models


class Foo(models.Model):
    name = models.CharField(default="a foo")
