django_app_labels = {"auth", "contenttypes", "admin", "sessions"}
foo_app_labels = {"foo"}.union(django_app_labels)
bar_app_labels = {"bar"}


class DatabaseRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label in bar_app_labels:
            return "bar"

    def db_for_write(self, model, **hints):
        if model._meta.app_label in bar_app_labels:
            return "bar"

    def allow_relation(self, obj1, obj2, **hints):
        if (
            obj1._meta.app_label in foo_app_labels
            and obj2._meta.app_label in foo_app_labels
        ):
            return True
        if (
            obj1._meta.app_label in bar_app_labels
            and obj2._meta.app_label in bar_app_labels
        ):
            return True
        return False

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label in foo_app_labels:
            return db == "default"
        if app_label in bar_app_labels:
            return db == "bar"
